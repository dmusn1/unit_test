// probador de headers
describe('solicitudes', () => {

  it("Debe de crear un empleado", function () {
      // DO a post request
      cy.request({
        url: "http://localhost:3000/employees", //solicitud http
        method: "POST",                           //utilizar post
        //body de la inserción
        body: {
          first_name: "Rutilia",
          last_name: "Cortez",
          email: "rcortez@cc.com",
        },
      }).then((response) => {
          //aserciones
        expect(response.status).to.eq(201);
        expect(response.body).to.have.property("id");
        //guardar el id del empleado
        const id = response.body.id;
        cy.wrap(id).as("id");
      });
    });

    it("Debemos de validar que se haya creado en la base de datos", () => {
      cy.request("GET", "http://localhost:3000/employees").then((response) => {
        //validate that the last record was created
        expect(response.body[response.body.length - 1].first_name).to.eq("Rutilia");
      });
    });

    it("Debemos de modificar al empleado con un nuevo correo", function () {
    
      cy.request("GET", "http://localhost:3000/employees").then((response) => {
        //validate that the last record was created
       const lastEmployeeId = response.body[response.body.length - 1].id;
       cy.request({
        url: `http://localhost:3000/employees/${lastEmployeeId}`,
         method: "PUT",
         body: {
           first_name: "Rutilia",
           last_name: "Desarrolladora",
           email: "nuevo@correo.com",
         },
        }).then((response) => {
          cy.log(response);
          expect(response.status).to.eq(200);
          expect(response.body).to.have.property("id");
        });

      });
     });

     it("Debemos de eliminar el registro creado", function () {
      cy.request({
        url: `http://localhost:3000/employees/${this.id}`,
        method: "DELETE",
      }).then((response) => {
        expect(response.status).to.eq(200);
      });
    });

    
})