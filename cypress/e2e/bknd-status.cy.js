describe('Probando statuses', () => {
    it('Debe de validar el status code exitoso', () => {
        cy.request('http://localhost:3000/employees/')
            .its('status')
            .should('eq', 200) //aserción
    })
    it('manejo de errores solicitud fallida',()=> {
        cy.request({url: 'http://localhost:3000/employees/10', failOnStatusCode: false}) //aceptamos el codigo de error
        .its('status')
        .should('eq', 404) // manejpo de erros
    })
});