describe('Asersion', () => {

    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })
    it('passes', () => {
        cy.visit('https://demoqa.com/automation-practice-form')
        cy.url().should('include', 'demoqa.com')
        cy.get('#firstName').should('be.visible').should('have.attr', 'placeholder','First Name')

    
    })
})
