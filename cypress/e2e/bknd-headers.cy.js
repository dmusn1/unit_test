// probador de headers
describe('Probando headers', () => {


    it('Debe de validar el header y el content type', () => {
        //llamando a una solicitud http
        cy.request('http://localhost:3000/employees')
            .its('headers') //busqueda de cabeceras
            .its('content-type') //busqueda de tipo de formanto
            .should('include', 'application/json') //aserción
    })

})